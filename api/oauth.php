<?php
    session_start();
    header('Content-Type: application/json');

    if(empty($_SESSION['token_oauth'])){
        if(!oAuth()){
            die(SendResult([], false, 1));
        }
    }else{
        if(empty($_SESSION['token_time'])) {
            if(!oAuth()){
                die(SendResult([], false, 1));
            }
        }else{
            if($_SESSION['token_time'] < time()){
                if(!oAuth()){
                    die(SendResult([], false, 1));
                }
            }
        }
    }

    function oAuth(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://test.gnzs.ru/oauth/get-token.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "X-Client-Id: 29123518",
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        if(!empty($response['access_token'])){
            $_SESSION['token_oauth'] = $response['access_token'];
            $_SESSION['token_time'] = time() + (20*60);
            return true;
        }else{
            return false;
        }
    }
    function SendResult($data, $status, $code){
        return json_encode([
            'data'=>$data,
            'status'=>$status,
            'code'=> $code,
        ], 256);
    }
