<?php
session_start();
header('Content-Type: application/json');

include_once "oauth.php";

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://dsamognzsru.amocrm.ru/api/v4/companies",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS =>json_encode(['data'=>[]], 256),
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer ".$_SESSION['token_oauth'],
    ),
));

$response = curl_exec($curl);
$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

$response = json_decode($response, true);
if(!empty($response['_embedded']['companies'])){
    $id = $response['_embedded']['companies'][0]['id'];
    die(SendResult(
        [
            'id'=>$id,
            'name'=>'Компания #'.$id
        ], true, $code));
}

die(SendResult([], false, $code));
