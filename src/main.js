import Vue from 'vue';

// plugins
import VueRouter from 'vue-router';
import VueCookie from 'vue-cookie';
import Vuex from 'vuex'

// main app
import App from './App.vue';

// routers
const index = () => import('./components/index.vue');

const routes = [
    {
        path: '/',
        name: 'index',
        component: index
    },
];

Vue.use(VueRouter);
Vue.use(VueCookie);
Vue.use(Vuex);

Vue.config.productionTip = false;

const store = new Vuex.Store({
    state: {
    }
});

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  render: h => h(App),
  router,
  store: store,
}).$mount('#app');
